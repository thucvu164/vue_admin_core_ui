import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')
const RegisterSuccess = () => import('@/views/pages/registerSuccess')

//Core
const UserProfile = () => import('@/views/core/UserProfile')
//admin
const AdminCompany = () => import('@/views/admin/Company')
const Pack = () => import('@/views/admin/Pack')
const PackType = () => import('@/views/admin/PackType')
const DashboardAdmin = () => import('@/views/admin/DashboardAdmin')
const ReportCodeForAdmin = () => import('@/views/admin/report/ReportCodeForAdmin')
const ReportScanForAdmin = () => import('@/views/admin/report/ReportScanForAdmin')
//Company
const RegisterPack = () => import('@/views/company/RegisterPack')
const ProductCompany = () => import('@/views/company/ProductCompany')
const BatchCompany = () => import('@/views/company/Batch')
const Active = () => import('@/views/company/Active')
const DashboardCompany = () => import('@/views/company/DashboardCompany')

Vue.use(Router);

let router = new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      meta: {title: 'Dashboard'},
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta: {title: 'Dashboard title'},
        },
        {
          path: 'user-profile',
          name: 'UserProfile',
          component: UserProfile,
          meta: {title: 'UserProfile'},
        },
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Login',
          component: Login,
          meta: {isToken: false},
        },
        {
          path: 'register',
          name: 'Register',
          component: Register,
          meta: {isToken: false},
        },
        {
          path: 'registerSuccess',
          name: 'RegisterSuccess',
          component: RegisterSuccess,
          meta: {isToken: false},
        }
      ]
    },
    {
      name: 'Admin',
      path: '/admin',
      redirect: '/dashboard',
      component: DefaultContainer,
      children: [
        {
          path: '',
          name: 'DashboardAdmin',
          component: DashboardAdmin
        },
        {
          path: 'company',
          name: 'CompanyAdmin',
          component: AdminCompany
        },
        {
          path: 'pack',
          name: 'AdminPack',
          component: Pack
        },
        {
          path: 'packtype',
          name: 'AdminPackType',
          component: PackType
        },
        {
          path: 'reportcode',
          name: 'ReportCodeForAdmin',
          component: ReportCodeForAdmin,
        },
        {
          path: 'reportscan',
          name: 'ReportScanForAdmin',
          component: ReportScanForAdmin,
        },
      ]
    },
    {
      name: 'Company',
      path: '/company',
      redirect: '/dashboard',
      component: DefaultContainer,
      children: [
        {
          path: '',
          name: 'DashboardCompany',
          component: DashboardCompany
        },
        {
          path: 'regPack',
          name: 'RegisterPack',
          component: RegisterPack
        },
        {
          path: 'product',
          name: 'ProductCompany',
          component: ProductCompany
        },
        {
          path: 'batch',
          name: 'BatchCompany',
          component: BatchCompany
        },
        {
          path: 'active',
          name: 'ActiveCompany',
          component: Active
        },
      ]
    }
  ],
});

router.beforeResolve((to, from, next) => {
  if (to.name) {
    if (to.meta.progress !== undefined) {
      let meta = to.meta.progress;
      router.app.$Progress.parseMeta(meta);
    }
    router.app.$Progress.start();
  }
  next();
});

router.afterEach((to, from) => {
  router.app.$Progress.finish();
});

router.beforeEach((to, from, next) => {
  if (to.name === 'Page404' || to.name === 'Page500') {
    document.title = router.app.$appName + ' | ' + router.app.$t('common.errorNotice');
    next();
    return;
  }
  if (to.meta.isToken === undefined){
    to.meta.isToken = true;
  }
  if (to.meta.isToken && (localStorage.token === undefined || localStorage.token === null || localStorage.token === '')){
    next({name: 'Login'});
    return;
  }
  if ((!to.meta.isToken) && (localStorage.token !== undefined && localStorage.token !== null && localStorage.token !== '')){

    next({name: 'Dashboard'});
    return;
  }
  if (router.app.$te('title.' + to.name)){
    document.title = router.app.$appName + ' | ' + router.app.$t('title.' + to.name);
  } else
    document.title = router.app.$appName + ' | ' + to.meta.title === undefined ? to.name : to.meta.title;
  next();
});

export default router;
