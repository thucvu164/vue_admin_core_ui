require('froala-editor/js/froala_editor.pkgd.min')
// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import i18n from './lang/__i18n'
import router from './router'
import FormError from './containers/FormError'
import vSelect from 'vue-select'
import Message from 'vue-m-message'
import VueProgressBar from 'vue-progressbar'
import DisplayDTMB from './containers/DisplayDTMB'
import Paginate from './shared/Paginate';
import VuejsDialog from "vuejs-dialog"
import 'vuejs-dialog/dist/vuejs-dialog.min.css'
import VueFroala from 'vue-froala-wysiwyg'
import DatePicker from 'vue2-datepicker'
import moment from 'moment'
// todo
// cssVars()
Vue.component('v-select', vSelect)
Vue.component('display-dt-mb', DisplayDTMB)
Vue.use(BootstrapVue);
Vue.use(Message);
Vue.component('paginate', Paginate)
Vue.use(VuejsDialog);
Vue.use(VueFroala)
Vue.use(DatePicker)
const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  inverse: false
};

Vue.use(VueProgressBar, options);
Vue.prototype.moment = moment
Vue.prototype.$toastr = function (type, message, isAlone = false, close = true) {
  if (this.$isNull(message)) return;
  if (isAlone) {
    this.$message.closeAll();
  }
  this.$message({
    message: message,
    type: type,
    showClose: close,
    zIndex: 10000
  });
};

Vue.prototype.$info = function (message, isAlone = false, close = true) {
  this.$toastr('info', message, isAlone, close);
};
Vue.prototype.$success = function (message, isAlone = false, close = true) {
  this.$toastr('success', message, isAlone, close);
};
Vue.prototype.$error = function (message, isAlone = false, close = true) {
  this.$toastr('error', message, isAlone, close);
};
Vue.prototype.$warning = function (message, isAlone = false, close = true) {
  this.$toastr('warning', message, isAlone, close);
};

Vue.prototype.$loading = function (message, onClose = null, center = false, timeDuration = 3000, isAlone = false) {
  if (isAlone) {
    this.$message.closeAll();
  }
  let l = this.$message.loading({
    message: message,
    onClose: (onClose == null ? function () {
    } : onClose),
    align: (center ? 'center' : ''),
    zIndex: 10000,
    duration: timeDuration
  });
};

Vue.prototype.$closeToastr = function(){
  this.$message.closeAll();
} ;

Vue.prototype.$saveFile = function(data, fileName){
  const url = window.URL.createObjectURL(new Blob([data]));
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', fileName);
  document.body.appendChild(link);
  link.click();
};

Vue.prototype.$isNull = function (property) {
  return property === undefined || property === null || property === '' || property === {} || property === [];
};

Vue.prototype.$valid = function (validObject) {
  let isValid = true;
  for (let i = 0; i < validObject.length; i++) {
    let property = validObject[i].property;
    if (!this.$checkError(property, validObject[i].error)) isValid = false;
  }
  return isValid;
};

Vue.prototype.$checkError = function (property, errorObject) {
  for (let j = 0; j < errorObject.length; j++) {
    switch (errorObject[j].type) {
      case 'NULL':
        if (this.$isNull(this[property])) {
          this.$changeState(property, errorObject[j]);
          return false;
        }
        break;
      case 'MAX-LENGTH':
        if (this[property] !== undefined && this[property] !== null && this[property].length > errorObject[j].data) {
          this.$changeState(property, errorObject[j]);
          return false;
        }
        break;
      case 'REGEX':
        if (this.$isNull(this[property])) continue;
        if (!errorObject[j].data.test(this[property])) {
          this.$changeState(property, errorObject[j]);
          return false;
        }
        break;
      case 'EMAIL':
        if (this.$isNull(this[property])) continue;
        if (!/^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/.test(this[property])) {
          this.$changeState(property, errorObject[j]);
          return false;
        }
        break;
      case 'SELECT-OBJECT':
        if (this.$isNull(this[property])) continue;
        let val = this[property][errorObject[j].key];
        for (let i = 0; i < errorObject[j].data.length; i++)
          if (errorObject[j].data[i] === val) {
            this.$changeState(property, errorObject[j]);
            return false;
          }
        break;
      case 'ARRAY-0':
        if (this.$isNull(this[property])) continue;
        if (Array.isArray(this[property]) && this[property].length === 0) {
          this.$changeState(property, errorObject[j]);
          return false;
        }
        break;
    }
  }
  this.$changeState(property, {errorDesc: ''}, true);
  return true;
};

Vue.prototype.$changeState = function (property, errorObj, type = false, normal = false) {
  let dom = this.$refs[property + 'Parent'];
  for (let i = 0; i < dom.childNodes.length; i++) {
    if (dom.childNodes[i].nodeType === Node.ELEMENT_NODE && dom.childNodes[i].id !== property + 'FormErrorID') {
      dom.childNodes[i].classList.remove('border-success');
      dom.childNodes[i].classList.remove('border-invalid');
      if (!normal) dom.childNodes[i].classList.add((type ? 'border-success' : 'border-invalid'));
    } else {
      dom.removeChild(dom.childNodes[i]);
      i--;
    }
  }
  if ((!normal) && (dom.lastElementChild.id !== property + 'FormErrorID')) {
    let testDiv = document.createElement("div");
    testDiv.setAttribute("id", property + 'FormErrorID');
    dom.appendChild(testDiv);
    const ComponentCtor = Vue.extend(FormError);
    const componentInstance = new ComponentCtor();
    componentInstance.setError(!type);
    let errorDesc = errorObj.errorDesc;
    if (errorObj['errorDesc_' + localStorage.locale] !== undefined) errorDesc = errorObj['errorDesc_' + localStorage.locale];
    componentInstance.setErrorDesc(errorDesc);
    componentInstance.setErrorID(property + 'FormErrorID');
    componentInstance.$mount('#' + property + 'FormErrorID');
  }

};

Vue.prototype.$reset = function (validObject) {
  for (let i = 0; i < validObject.length; i++) {
    let property = validObject[i].property;
    !this.$changeState(property, '', false, true);
  }
};

Vue.prototype.$getLang = function(){
  return localStorage.locale === 'vi' ? {
    'days': ['CN', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
    'months': ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
    'pickers': ['7 ngày tiếp', '30 ngày tiếp', '7 ngày trước', '30 ngày trước'],
    'placeholder': {
      'date': 'Chọn ngày',
      'dateRange': 'Chọn khoảng ngày'
    }
  } : localStorage.locale;
};

Vue.prototype.$getFormat = function(type = null){
  if (type == null) {
    return localStorage.locale === 'vi' ? 'DD/MM/YYYY' : (localStorage.locale === 'en' ? 'MM/DD/YYYY' : '');
  }
};



Vue.prototype.$appName = 'Check Tax';
/* eslint-disable no-new */
// export const serverBus = new Vue();

Vue.prototype.$safeRef = function () {
  let ref = '';
  while (ref === ''){
    ref = 'ref_' + Math.random().toString(36).substr(2, 16);
    if (this.$refs[ref] !== undefined) ref = '';
  }
  return ref;
};

export const serverBus = new Vue({
// new Vue({
  el: '#app',
  i18n,
  router,
  template: '<App/>',
  components: {
    App,
  },
});

