const webpack = require('webpack');
module.exports = {
  baseUrl: './',
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
      })
    ]
  }
};
